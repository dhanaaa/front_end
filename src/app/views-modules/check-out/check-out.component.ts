import { Component, OnInit } from '@angular/core';
import { BsModalRef} from 'ngx-bootstrap';

import {Subject} from 'rxjs';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {
  public onClose: Subject<boolean>;
  constructor(  public bsModalRef: BsModalRef, ) {
    this.onClose = new Subject();
  }

  ngOnInit() {
  }
  onCloseModal(response: any) {
    if (response) {
      this.onClose.next(response);
    }
    this.bsModalRef.hide();
  }
}
