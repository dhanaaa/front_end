import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.scss']
})
export class ProductsViewComponent implements OnInit, OnDestroy {
  private sub: any;
  public routeUrl: string;
  public routerId: number;

  constructor(private route: ActivatedRoute) {
    this.sub = this.route.data.subscribe(v => {
      if (v.Id > 0) {
        this.routeUrl = 'category-' + v.Id;
        this.routerId = v.Id;
      }
    });
  }

  ngOnInit() {
    console.log(this.routeUrl);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
