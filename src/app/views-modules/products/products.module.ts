import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ProductsViewComponent } from './products-view/products-view.component';
import { DetailsComponent } from './details/details.component';


@NgModule({
  declarations: [ProductsComponent, ProductsViewComponent, DetailsComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule
  ]
})
export class ProductsModule { }
