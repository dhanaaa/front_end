import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductsComponent} from './products.component';
import {ProductsViewComponent} from './products-view/products-view.component';
import {DetailsComponent} from './details/details.component';


const routes: Routes = [{
  path: '',
  component: ProductsComponent,
  children: [
  {path: 'category-1', component: ProductsViewComponent, data : {Id: 1}},
  {path: 'category-2', component: ProductsViewComponent, data : {Id: 2}},
  {path: 'category-3', component: ProductsViewComponent, data : {Id: 3}},
  {path: 'details', component: DetailsComponent }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
