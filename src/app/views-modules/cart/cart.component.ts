import { Component, OnInit } from '@angular/core';
import { BsModalRef} from 'ngx-bootstrap';
import {GlobalVariable} from '../../core/com-classes';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public onClose: Subject<boolean>;
  public cartCount: number;
  constructor( private globalVariable: GlobalVariable , public bsModalRef: BsModalRef, ) {
    this.onClose = new Subject();
  }

  ngOnInit() {
  }
  onCloseModal(response: any) {
    if (response) {
      this.onClose.next(response);
    }
    this.bsModalRef.hide();
  }
}
