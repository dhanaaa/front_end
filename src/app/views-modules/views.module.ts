import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsRoutingModule } from './views-routing.module';
import { ViewsComponent } from './views.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ItemCardComponent } from './item-card/item-card.component';

// import { ProfileComponent } from './profile/profile.component';
// import { CheckOutComponent } from './check-out/check-out.component';
// import {CartComponent} from './cart/cart.component';


@NgModule({
  declarations: [ViewsComponent, HomeComponent, AboutComponent, ContactComponent,ItemCardComponent,],
  imports: [
    CommonModule,
    ViewsRoutingModule
  ]
})
export class ViewsModule { }
