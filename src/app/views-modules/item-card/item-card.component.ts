import { Component, OnInit, Input } from '@angular/core';
import {GlobalVariable} from '../../core/com-classes';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {
  public cartCount: number;
  @Input() item: any;

  constructor(private globalVariable: GlobalVariable) { }

  ngOnInit() {
    console.log('item', this.item)
    this.cartCount = 5;
    this.globalVariable.cartCount = this.cartCount;
  }
  public addToCart() {
    this.cartCount++;
    console.log(this.cartCount);
    this.globalVariable.cartCount = this.cartCount;
  }

}
