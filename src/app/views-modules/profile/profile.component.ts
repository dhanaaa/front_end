import { Component, OnInit } from '@angular/core';
import { BsModalRef} from 'ngx-bootstrap';

import {Subject} from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public onClose: Subject<boolean>;
  constructor(  public bsModalRef: BsModalRef, ) {
    this.onClose = new Subject();
  }

  ngOnInit() {
  }
  onCloseModal(response: any) {
    if (response) {
      this.onClose.next(response);
    }
    this.bsModalRef.hide();
  }

}
