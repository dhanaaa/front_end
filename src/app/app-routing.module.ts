import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FullLayoutComponent} from './containers/full-layout/full-layout.component';


const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    loadChildren: './views-modules/views.module#ViewsModule',

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
