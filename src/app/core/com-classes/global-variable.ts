import { Injectable } from '@angular/core';

@Injectable()
export class GlobalVariable {
  private _cartCount = 0;

  get cartCount(): number {
    return this._cartCount;
  }
  set cartCount(value: number) {
    this._cartCount = value;
  }

}
