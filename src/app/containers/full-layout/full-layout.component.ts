import { Component, OnInit } from '@angular/core';
import {GlobalVariable} from '../../core/com-classes';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {CartComponent} from '../../views-modules/cart/cart.component';
import {CheckOutComponent} from '../../views-modules/check-out/check-out.component';
import {ProfileComponent} from '../../views-modules/profile/profile.component';
import {JwksValidationHandler, OAuthService} from 'angular-oauth2-oidc';
import {authConfig} from '../../logingConfig';
declare var jQuery: any;

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.scss']
})
export class FullLayoutComponent implements OnInit {
  today: Date = new Date();
  public cartCount: number;
  bsModalRef: BsModalRef;
  gsModalRef: BsModalRef;

  constructor(private oathService: OAuthService , private modalService: BsModalService, private globalVariable: GlobalVariable, ) {
    this.configureSingleSignOn();
  }

  ngOnInit() {
    (function ($) {
      $(window).on('scroll', function () {
        if ( $(window).scrollTop() > 10 ) {
          $('.navbar').addClass('active');
        } else {
          $('.navbar').removeClass('active');
        }
      });
    })(jQuery);

    setTimeout(() => {
      this.cartCount = this.globalVariable.cartCount;
      console.log(this.globalVariable.cartCount);
    }, 0);
  }
public onClickCart() {
    this.openCartModal();
}
  configureSingleSignOn() {
    this.oathService .configure(authConfig);
    this. oathService .tokenValidationHandler = new JwksValidationHandler();
    this.oathService .loadDiscoveryDocumentAndTryLogin();
  }

  onClickLoging() {
    this.oathService.initImplicitFlow();
  }

  onClickLogOut() {
    this.oathService.logOut();
  }

  get token() {
    const claims: any = this.oathService.getIdentityClaims();
    return claims ? claims : null;
  }


  private openCartModal() {
    const modalConfig: any = {
      class: 'my-modal',
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true,
    };

    this.bsModalRef = null;
    this.bsModalRef = this.modalService.show(CartComponent, modalConfig);
    this.bsModalRef.setClass('modal-lg');
    // this.bsModalRef.content.action = this.action;
    // this.bsModalRef.content.orderDetail = Object.assign({}, this.orderDetail || {});
    this.bsModalRef.content.onClose.subscribe(response => {
      if (response.Id) {
          console.log('modal closed');
      }
    });
  }

  public onClickCheckOut() {
    this.openCheckOutModal();
  }

  private openCheckOutModal() {
    const modal2Config: any = {
      class: 'my-modal',
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true,
    };

    this.gsModalRef = null;
    this.gsModalRef = this.modalService.show(CheckOutComponent, modal2Config);
    this.gsModalRef.setClass('modal-lg');
    // this.bsModalRef.content.action = this.action;
    // this.bsModalRef.content.orderDetail = Object.assign({}, this.orderDetail || {});
    this.gsModalRef.content.onClose.subscribe(response => {
      if (response) {
        console.log('modal closed');
      }
    });
  }

  public onClickprofile() {
    this.openCheckprofile();
  }

  private openCheckprofile() {
    const modal2Config: any = {
      class: 'my-modal-profile',
      animated: true,
      keyboard: true,
      backdrop: false,
      ignoreBackdropClick: true,
    };

    this.gsModalRef = null;
    this.gsModalRef = this.modalService.show(ProfileComponent, modal2Config);
    this.gsModalRef.setClass('modal-sm');
    // this.bsModalRef.content.action = this.action;
    // this.bsModalRef.content.orderDetail = Object.assign({}, this.orderDetail || {});
    this.gsModalRef.content.onClose.subscribe(response => {
      if (response) {
        console.log('modal closed');
      }
    });
  }

}
