import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './containers/full-layout/full-layout.component';
import {GlobalVariable} from './core/com-classes';
import {ModalModule} from 'ngx-bootstrap/modal';
import {CartComponent} from './views-modules/cart/cart.component';
import {CheckOutComponent} from './views-modules/check-out/check-out.component';
import { HttpClientModule } from '@angular/common/http';
import { AvatarModule } from 'ngx-avatar';
import {ProfileComponent} from './views-modules/profile/profile.component';
import { OAuthModule} from 'angular-oauth2-oidc';


@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    ProfileComponent,
    CartComponent,
    CheckOutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AvatarModule,
    ModalModule.forRoot(),
    OAuthModule.forRoot()
  ],
  providers: [GlobalVariable, ],
  entryComponents: [CartComponent, CheckOutComponent, ProfileComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
